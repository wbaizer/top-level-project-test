import React, { Component } from 'react';
import InitTimer from './InitTimer';
import DisplayPane from './DisplayPane'
import Speed from './Speed'
import { SPEED_1 } from './constant'
class Home extends Component {
    state = {
        countTime: 0,
        startTimer: false,
        grade: SPEED_1
    }
    startCount = (countTime, stared) => {
        this.setState({
            countTime: countTime * 60,
            startTimer: stared
        })
    }
    handleGrade = (grade) => {
        console.log(grade)
        this.setState({
            grade: grade
        })
    }
    render() {
        const { countTime, startTimer, grade } = this.state
        return (
            <div style={{ textAlign: "center" }}>
                <div className="home">
                    <InitTimer startTimer={this.startCount} />
                    <DisplayPane countTime={countTime} startTimer={startTimer} grade={grade} />
                    <Speed handleGrade={this.handleGrade} />
                </div>
            </div>
        );
    }
}

export default Home;