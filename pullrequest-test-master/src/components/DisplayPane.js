import React, { Component } from 'react';
import classNames from 'classnames'
import Fab from '@material-ui/core/Fab';
import PlayCircleFilledWhiteIcon from '@material-ui/icons/PlayCircleFilledWhite';
import PauseCircleOutlineIcon from '@material-ui/icons/PauseCircleOutline';
import { GET_TEXT, REMAIN_20_SECONDS, REMAIN_10_SECONDS, REMAIN_HALF, GET_MINUTE, GET_SECOND } from './constant'
class DisplayPane extends Component {
    state = {
        counter: 0,
        counting: true
    }
    componentDidMount() {
        this.interval = setInterval(() => {
            const { startTimer, countTime, grade } = this.props
            const { counter, counting } = this.state
            if (startTimer) {
                if (counter < countTime && counting) {
                    const degree = counter + grade > countTime ? countTime : counter + grade
                    this.setState({ counter: degree });
                }
            } else {
                this.setState({
                    counter: 0,
                    counting: true
                });
            }
        }, 1000);

    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }
    setPause = (value) => {
        const { startTimer } = this.props
        if (startTimer) {
            this.setState({
                counting: value
            })
        }
    }
    render() {
        let { counter, counting } = this.state
        let countTime = this.props.countTime

        let alertTextClass = classNames({
            normal_text: true,
            blink: REMAIN_10_SECONDS(countTime, counter),
            red_warning: REMAIN_20_SECONDS(countTime, counter),
            text_disable: REMAIN_HALF(countTime, counter)

        });
        const minute = GET_MINUTE(countTime, counter)
        const second = GET_SECOND(countTime, counter)
        const text = GET_TEXT(countTime, counter)
        return (
            <div className="displaypane">
                <h6 className={alertTextClass}>{text}</h6>
                <div className="row">
                    <p className="timepad col-8">{minute}:{second}
                    </p>
                    {counting ? (
                        <Fab
                            color="primary"
                            aria-label="add"
                            style={{ margin: 'auto' }}
                            onClick={() => this.setPause(false)}
                        >
                            <PauseCircleOutlineIcon style={{ fontSize: '30px' }} />
                        </Fab>
                    ) : (
                            <Fab
                                color="primary"
                                aria-label="add"
                                style={{ margin: 'auto' }}
                                onClick={() => this.setPause(true)}>
                                <PlayCircleFilledWhiteIcon style={{ fontSize: '30px' }} />
                            </Fab>
                        )
                    }
                </div>
            </div>
        );
    }
}

export default DisplayPane;