import React, { Component } from 'react';
import { INIT_COUNT_TIME } from './constant'

import './Home.scss'
class InitTimer extends Component {
    state = {
        countTime: INIT_COUNT_TIME,
        started: false
    }
    handleTimer = () => {
        const { countTime, started } = this.state
        try {
            let i_countTime = parseInt(countTime)
            if (!isNaN(i_countTime)) {
                this.props.startTimer(i_countTime, !started)
                this.setState({
                    started: !started
                })
            } else {
                alert('Please enter time')
            }
        } catch (err) {

        }

    }
    handleTimeChange = (evt) => {
        let value = evt.target.value
        //        if (isNaN(parseInt(value))) value = INIT_COUNT_TIME
        this.setState({
            countTime: evt.target.value
        })
    }
    render() {
        const { countTime, started } = this.state
        const btnTxt = started ? 'STOP' : 'START'
        return (
            <div className="inittimer row">
                <p className="col-4" className="p_class">Countdown:</p>
                <input
                    type="number"
                    className="input_class col-4 form-control"
                    placeholder="(Min)"
                    value={countTime}
                    onChange={this.handleTimeChange}
                />
                <button
                    type="button"
                    className="btn_class btn btn-success"
                    onClick={this.handleTimer}
                >{btnTxt}</button>
            </div>
        );
    }
}

export default InitTimer;