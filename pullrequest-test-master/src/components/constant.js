export const INIT_COUNT_TIME = 1

export const SPEED_1 = 1
export const SPEED_1_5 = 1.5
export const SPEED_2 = 2

export const SELECTED_ELEMENT_1 = 0
export const SELECTED_ELEMENT_2 = 1
export const SELECTED_ELEMENT_3 = 2

export const REMAIN_10_SECONDS = (countTime, counter) => {
    if (countTime === 0) return false
    if (countTime - counter < 11) return true;
    else return false
};
export const REMAIN_20_SECONDS = (countTime, counter) => {
    if (countTime === 0) return false
    if (countTime - counter < 21) return true;
    else return false
};
export const REMAIN_HALF = (countTime, counter) => {
    if (countTime === 0) return true
    if (counter < (countTime / 2 - 1)) return true
    else return false
};

export const GET_TEXT = (countTime, counter) => {
    const val = Math.floor(countTime - counter)
    console.log(val)
    if (val === 0) {
        return "Time's up"
    } else if (counter > (countTime / 2 - 1)) {
        return 'More than halfway there!'
    }
}
export const GET_MINUTE = (countTime, counter) => {
    let countdown = countTime - counter
    let minute = Math.floor(countdown / 60);
    minute += ''
    if (minute.length === 1) {
        minute = '0' + minute
    }
    return minute
}
export const GET_SECOND = (countTime, counter) => {
    let countdown = countTime - counter
    let sec = Math.floor(countdown % 60);
    sec += ''
    if (sec.length === 1) {
        sec = '0' + sec
    }
    return sec
}