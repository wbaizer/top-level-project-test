import React, { Component } from 'react';
import classNames from 'classnames'
import { SPEED_1, SPEED_1_5, SPEED_2, SELECTED_ELEMENT_1, SELECTED_ELEMENT_2, SELECTED_ELEMENT_3} from './constant'

class Speed extends Component {
    state = {
        selected: SELECTED_ELEMENT_1,
        degree: SPEED_1
    }
    select = (val) => {
        let { degree } = this.state
        if (val === SELECTED_ELEMENT_1) {
            degree = 1
        } else if (val === SELECTED_ELEMENT_2) {
            degree = SPEED_1_5
        } else if (val === SELECTED_ELEMENT_3) {
            degree = SPEED_2
        }
        this.props.handleGrade(degree)
        this.setState({
            selected: val,
            degree: degree
        })
    }
    render() {
        let { selected } = this.state
        let selectedClass1 = classNames('degree', {
            selectedClass: selected === SELECTED_ELEMENT_1
        })
        let selectedClass2 = classNames('degree', {
            selectedClass: selected === SELECTED_ELEMENT_2
        })
        let selectedClass3 = classNames('degree', {
            selectedClass: selected === SELECTED_ELEMENT_3
        })
        return (
            <div className="row speed">
                <div className="col-4">
                    <div className={selectedClass1} onClick={() => this.select(0)}>
                        <p className="pClass">{SPEED_1}x</p>
                    </div>
                </div>
                <div className="col-4">
                    <div className={selectedClass2} onClick={() => this.select(1)}>
                        <p className="pClass">{SPEED_1_5}x</p>
                    </div>
                </div>
                <div className="col-4">
                    <div className={selectedClass3} onClick={() => this.select(2)}>
                        <p className="pClass">{SPEED_2}x</p>
                    </div>
                </div>
            </div>
        );
    }
}

export default Speed;